
#include <GL\glew.h>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include <cstdio>
#include <cassert>

#include "GL_framework.h"

#include "my_render_code.h"

float travelCameraX = 0;
float cameraZ = 11;
float cameraZ2 = 6;
float operture = (65.f);
float myFOV = glm::radians(operture);
float myWidth;
float myHeight;
int cameraType = 3;

///////// fw decl
namespace ImGui {
	void Render();
}
namespace Box {
	void setupCube();
	void cleanupCube();
	void drawCube();
}
namespace Axis {
	void setupAxis();
	void cleanupAxis();
	void drawAxis();
}

namespace Sphere
{
	void createSphereShaderAndProgram();
	void cleanupSphereShaderAndProgram();
	void setupSphere(glm::vec3 pos, float radius);
	void cleanupSphere();
	void updateSphere(glm::vec3 pos, float radius);
	void drawSphere();
	void drawTrillionSpheres(glm::vec3 newPosition);
}

namespace Cube {
	void setupCube();
	void cleanupCube();
	void updateCube(const glm::mat4& transform);
	void drawCube();
	void drawCube(glm::vec3 neWPosition);
	void draw2Cubes(double currentTime);
	void drawScene(double currentTime);
}


// First shader || Background changing color
namespace MyFirstShader
{
	void myInitCode(void);
	GLuint myShaderCompile(void);

	void myCleanupCode(void);
	void myRenderCode(double currentTime);

	// unsigned int opengl || guarda direcciones de memoria de nuestra tarjeta grafica.
	GLuint myRenderProgram;
	GLuint myVAO;
}

namespace RenderVars {
	float FOV = glm::radians(65.f);
	const float zNear = 1.f;
	const float zFar = 50.f;
	
	
	glm::mat4 _projection;
	glm::mat4 _modelView;
	glm::mat4 _MVP;
	glm::mat4 _inv_modelview;
	glm::vec4 _cameraPoint;

	struct prevMouse {
		float lastx, lasty;
		MouseEvent::Button button = MouseEvent::Button::None;
		bool waspressed = false;
	} prevMouse;
	
	float panv[3] = { 0.f, -5.f, -15.f };
	float rota[2] = { 0.f, 0.f };
}
namespace RV = RenderVars;

void GLResize(int width, int height) {
	glViewport(0, 0, width, height);
	if (height != 0) RV::_projection = glm::perspective(myFOV, (float)width / (float)height, RV::zNear, RV::zFar);
	else RV::_projection = glm::perspective(RV::FOV, 0.f, RV::zNear, RV::zFar);
	
}

void GLmousecb(MouseEvent ev) {
	if (RV::prevMouse.waspressed && RV::prevMouse.button == ev.button) {
		float diffx = ev.posx - RV::prevMouse.lastx;
		float diffy = ev.posy - RV::prevMouse.lasty;
		switch (ev.button) {
		case MouseEvent::Button::Left: // ROTATE
			RV::rota[0] += diffx * 0.005f;
			RV::rota[1] += diffy * 0.005f;
			break;
		case MouseEvent::Button::Right: // MOVE XY
			RV::panv[0] += diffx * 0.03f;
			RV::panv[1] -= diffy * 0.03f;
			break;
		case MouseEvent::Button::Middle: // MOVE Z
			RV::panv[2] += diffy * 0.05f;
			break;
		default: break;
		}
	}
	else {
		RV::prevMouse.button = ev.button;
		RV::prevMouse.waspressed = true;
	}
	RV::prevMouse.lastx = ev.posx;
	RV::prevMouse.lasty = ev.posy;
}

void GLinit(int width, int height) {
	glViewport(0, 0, width, height);
	glClearColor(0.2f, 0.2f, 0.2f, 1.f);
	glClearDepth(1.f);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	myWidth =(float)width;
	myHeight =(float)height;

	const float orthoScale = 80;

	//RV::_projection = glm::perspective(myFOV, (float)width / (float)height, RV::zNear, RV::zFar);	// Perspective camera
	// RV::_projection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, RV::zNear, RV::zFar);						// Orthogonal camera
	RV::_projection = glm::ortho(-(float)width/orthoScale, (float)width/orthoScale, -(float)height/orthoScale, (float)width/orthoScale, RV::zNear, RV::zFar);						// Orthogonal camera


	// Setup shaders & geometry
	/*Box::setupCube();
	Axis::setupAxis();
	Cube::setupCube();*/

	//MyFirstShader::myInitCode();

	Cube::setupCube();
	Axis::setupAxis();
	Box::setupCube();
}

void GLcleanup() {
	/*Box::cleanupCube();
	Axis::cleanupAxis();
	Cube::cleanupCube();
	*/

	//MyFirstShader::myCleanupCode();
	Cube::cleanupCube();
	Box::cleanupCube();
	Axis::cleanupAxis();

}

void GLrender(double currentTime) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// ****
	// **** Descomentar si no utilizamos el LOOK AT 
	// ****
	/*RV::_modelView = glm::mat4(1.f);
	
	RV::_modelView = glm::translate(RV::_modelView, glm::vec3(RV::panv[0], RV::panv[1], RV::panv[2]));*/
	
	//Bad camera travel
	/*travelCameraX += 0.1;
	travelCameraX = fmod(travelCameraX, 6);
	RV::_modelView = glm::translate(RV::_modelView, glm::vec3(travelCameraX-3, -4.f, -10.f));*/
	//End
	//RV::_modelView = glm::rotate(RV::_modelView, RV::rota[1], glm::vec3(1.f, 0.f, 0.f));
	//RV::_modelView = glm::rotate(RV::_modelView, RV::rota[0], glm::vec3(0.f, 1.f, 0.f));

	RV::_MVP = RV::_projection * RV::_modelView;
	switch (cameraType)
	{
	case 1:
		RV::_modelView = glm::mat4(1.f);

		RV::_modelView = glm::translate(RV::_modelView, glm::vec3(RV::panv[0], RV::panv[1], RV::panv[2]));
		travelCameraX += 0.06f;
		travelCameraX = fmod(travelCameraX, 8);
		RV::_modelView = glm::translate(RV::_modelView, glm::vec3(travelCameraX - 4, -1.f, -10.f));
		break;
	case 2:
		RV::_projection = glm::perspective(myFOV, (float)myWidth / (float)myHeight, RV::zNear, RV::zFar);
		if (cameraZ>4.5f) {
			cameraZ -= 0.05;
		}
		else if (operture<100) {

			operture += 0.5f;
			myFOV = glm::radians(operture);
		}

		RV::_modelView = glm::lookAt
		(
			glm::vec3(-0.65f, 3.f, cameraZ),						// Camera position
			glm::vec3(-0.65, 3.f, 2.3),		// Target position
			glm::vec3(0.f, 1.f, 0.f)						// Who knows
		);
		break;
	case 3:
		RV::_projection = glm::perspective(myFOV, (float)myWidth / (float)myHeight, RV::zNear, RV::zFar);
		if (cameraZ2>3.8f) {
			cameraZ2 -= 0.01f*2.5f;
			operture += 0.25f*2.5f;
			myFOV = glm::radians(operture);
		}



		RV::_modelView = glm::lookAt
		(
			glm::vec3(-0.65f, 3.f, cameraZ2),						// Camera position
			glm::vec3(-0.65, 3.f, 2.3),		// Target position
			glm::vec3(0.f, 1.f, 0.f)						// Who knows
		);
		
		break;
	default:
		if (cameraZ2>3.8f) {
		cameraZ2 -= 0.01f*2.5f;
		operture += 0.25f*2.5f;
		myFOV = glm::radians(operture);
	}
	
		
	
	RV::_modelView = glm::lookAt
	(
		glm::vec3(-0.65f, 3.f, cameraZ2),						// Camera position
		glm::vec3(-0.65, 3.f, 2.3),		// Target position
		glm::vec3(0.f, 1.f, 0.f)						// Who knows
	);
		break;
	}
	//RV::_projection = glm::perspective(myFOV, (float)myWidth / (float)myHeight, RV::zNear, RV::zFar);


	/****************** CAMBIO DE COLOR ***********************/
	//const GLfloat red[] = { (float)sin(currentTime) * 0.5f + 0.5f, (float)cos(currentTime) * 0.5f + 0.5f, 0.f, 1.f };
	//glClearBufferfv(GL_COLOR, 0, red);

	/****************** TRAVELING CAMERA ***********************/
	//RV::panv[1] = fmod(currentTime, 5);				// Posicion medio camara
	//RV::panv[0] = -fmod(currentTime, 5);					// Traveling latera

	//RV::panv[1] = 0.f;				// Posicion medio camara
	//RV::rota[1] = 1.5708;			// 1.5708f radians = 90 degrees
	//RV::rota[0] = fmod(currentTime, 5);

	//Cube::drawCube();

	/****************** LOOK AT ***********************/
	// Descomentar el RV::_modelView de arriba si no se usa esta funcion
	/*
	RV::_modelView = glm::lookAt
	(
	glm::vec3(0.f, 4.f, -8.f),						// Camera position
	glm::vec3(fmod(currentTime, 5), 2.f, 3.f),		// Target position
	glm::vec3(0.f, 1.f, 0.f)						// Who knows
	);
	*/
	
	/****************** ZOOM OBJECT****************/
	// Descomentar el RV::_modelView de arriba si no se usa esta funcion
	/*if (cameraZ>4.5f){ 
		cameraZ -= 0.05; 
	}
	else if(operture<100) {
		
		operture += 0.5f;
		myFOV = glm::radians(operture);
	}
		
	RV::_modelView = glm::lookAt
	(
	glm::vec3(-0.65f, 3.f, cameraZ),						// Camera position
	glm::vec3(-0.65, 3.f, 2.3),		// Target position
	glm::vec3(0.f, 1.f, 0.f)						// Who knows
	);*/


	/****************** DOLLY****************/
	// Descomentar el RV::_modelView de arriba si no se usa esta funcion
	/*if (cameraZ2>3.8f) {
		cameraZ2 -= 0.01f*2.5f;
		operture += 0.25f*2.5f;
		myFOV = glm::radians(operture);
	}
	
		
	
	RV::_modelView = glm::lookAt
	(
		glm::vec3(-0.65f, 3.f, cameraZ2),						// Camera position
		glm::vec3(-0.65, 3.f, 2.3),		// Target position
		glm::vec3(0.f, 1.f, 0.f)						// Who knows
	);


	/****************** RENDER CUBOS ***********************/
	// render code

	/*
	Box::drawCube();
	Axis::drawAxis();
	Cube::drawCube();
	Cube::draw2Cubes(currentTime);
	*/


	// DOLLY SCENE
	Box::drawCube();
	//Axis::drawAxis();

	//Cube::drawCube(glm::vec3(0, 1, 2));
	//Sphere::drawTrillionSpheres(glm::vec3(0, 1, -2));

	Cube::drawScene(currentTime);


	ImGui::Render();
}

//////////////////////////////////// COMPILE AND LINK
GLuint compileShader(const char* shaderStr, GLenum shaderType, const char* name = "") {
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &shaderStr, NULL);
	glCompileShader(shader);
	GLint res;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &res);
	if (res == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &res);
		char *buff = new char[res];
		glGetShaderInfoLog(shader, res, &res, buff);
		fprintf(stderr, "Error Shader %s: %s", name, buff);
		delete[] buff;
		glDeleteShader(shader);
		return 0;
	}
	return shader;
}
void linkProgram(GLuint program) {
	glLinkProgram(program);
	GLint res;
	glGetProgramiv(program, GL_LINK_STATUS, &res);
	if (res == GL_FALSE) {
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &res);
		char *buff = new char[res];
		glGetProgramInfoLog(program, res, &res, buff);
		fprintf(stderr, "Error Link: %s", buff);
		delete[] buff;
	}
}

////////////////////////////////////////////////// BOX
namespace Box {
	GLuint cubeVao;
	GLuint cubeVbo[2];
	GLuint cubeShaders[2];
	GLuint cubeProgram;

	float cubeVerts[] = {
		// -5,0,-5 -- 5, 10, 5
		-5.f,  0.f, -5.f,
		5.f,  0.f, -5.f,
		5.f,  0.f,  5.f,
		-5.f,  0.f,  5.f,
		-5.f, 10.f, -5.f,
		5.f, 10.f, -5.f,
		5.f, 10.f,  5.f,
		-5.f, 10.f,  5.f,
	};
	GLubyte cubeIdx[] = {
		1, 0, 2, 3, // Floor - TriangleStrip
		0, 1, 5, 4, // Wall - Lines
		1, 2, 6, 5, // Wall - Lines
		2, 3, 7, 6, // Wall - Lines
		3, 0, 4, 7  // Wall - Lines
	};

	const char* vertShader_xform =
		"#version 330\n\
		in vec3 in_Position;\n\
		uniform mat4 mvpMat;\n\
		void main() {\n\
			gl_Position = mvpMat * vec4(in_Position, 1.0);\n\
		}";
	const char* fragShader_flatColor =
		"#version 330\n\
		out vec4 out_Color;\n\
		uniform vec4 color;\n\
		void main() {\n\
			out_Color = color;\n\
		}";

	void setupCube() {
		glGenVertexArrays(1, &cubeVao);
		glBindVertexArray(cubeVao);
		glGenBuffers(2, cubeVbo);

		glBindBuffer(GL_ARRAY_BUFFER, cubeVbo[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 24, cubeVerts, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeVbo[1]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte) * 20, cubeIdx, GL_STATIC_DRAW);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		cubeShaders[0] = compileShader(vertShader_xform, GL_VERTEX_SHADER, "cubeVert");
		cubeShaders[1] = compileShader(fragShader_flatColor, GL_FRAGMENT_SHADER, "cubeFrag");

		cubeProgram = glCreateProgram();
		glAttachShader(cubeProgram, cubeShaders[0]);
		glAttachShader(cubeProgram, cubeShaders[1]);
		glBindAttribLocation(cubeProgram, 0, "in_Position");
		linkProgram(cubeProgram);
	}
	void cleanupCube() {
		glDeleteBuffers(2, cubeVbo);
		glDeleteVertexArrays(1, &cubeVao);

		glDeleteProgram(cubeProgram);
		glDeleteShader(cubeShaders[0]);
		glDeleteShader(cubeShaders[1]);
	}
	void drawCube() {
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
		// FLOOR
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.6f, 0.6f, 0.6f, 1.f);
		glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, 0);
		// WALLS
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, 0.f, 0.f, 1.f);
		glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 4));
		glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 8));
		glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 12));
		glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 16));

		glUseProgram(0);
		glBindVertexArray(0);
	}
}

////////////////////////////////////////////////// AXIS
namespace Axis {
	GLuint AxisVao;
	GLuint AxisVbo[3];
	GLuint AxisShader[2];
	GLuint AxisProgram;

	float AxisVerts[] = {
		0.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 1.0
	};
	float AxisColors[] = {
		1.0, 0.0, 0.0, 1.0,
		1.0, 0.0, 0.0, 1.0,
		0.0, 1.0, 0.0, 1.0,
		0.0, 1.0, 0.0, 1.0,
		0.0, 0.0, 1.0, 1.0,
		0.0, 0.0, 1.0, 1.0
	};
	GLubyte AxisIdx[] = {
		0, 1,
		2, 3,
		4, 5
	};
	const char* Axis_vertShader =
		"#version 330\n\
in vec3 in_Position;\n\
in vec4 in_Color;\n\
out vec4 vert_color;\n\
uniform mat4 mvpMat;\n\
void main() {\n\
	vert_color = in_Color;\n\
	gl_Position = mvpMat * vec4(in_Position, 1.0);\n\
}";
	const char* Axis_fragShader =
		"#version 330\n\
in vec4 vert_color;\n\
out vec4 out_Color;\n\
void main() {\n\
	out_Color = vert_color;\n\
}";

	void setupAxis() {
		glGenVertexArrays(1, &AxisVao);
		glBindVertexArray(AxisVao);
		glGenBuffers(3, AxisVbo);

		glBindBuffer(GL_ARRAY_BUFFER, AxisVbo[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 24, AxisVerts, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, AxisVbo[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 24, AxisColors, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 4, GL_FLOAT, false, 0, 0);
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, AxisVbo[2]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte) * 6, AxisIdx, GL_STATIC_DRAW);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		AxisShader[0] = compileShader(Axis_vertShader, GL_VERTEX_SHADER, "AxisVert");
		AxisShader[1] = compileShader(Axis_fragShader, GL_FRAGMENT_SHADER, "AxisFrag");

		AxisProgram = glCreateProgram();
		glAttachShader(AxisProgram, AxisShader[0]);
		glAttachShader(AxisProgram, AxisShader[1]);
		glBindAttribLocation(AxisProgram, 0, "in_Position");
		glBindAttribLocation(AxisProgram, 1, "in_Color");
		linkProgram(AxisProgram);
	}
	void cleanupAxis() {
		glDeleteBuffers(3, AxisVbo);
		glDeleteVertexArrays(1, &AxisVao);

		glDeleteProgram(AxisProgram);
		glDeleteShader(AxisShader[0]);
		glDeleteShader(AxisShader[1]);
	}
	void drawAxis() {
		glBindVertexArray(AxisVao);
		glUseProgram(AxisProgram);
		glUniformMatrix4fv(glGetUniformLocation(AxisProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
		glDrawElements(GL_LINES, 6, GL_UNSIGNED_BYTE, 0);

		glUseProgram(0);
		glBindVertexArray(0);
	}
}

////////////////////////////////////////////////// SPHERE
namespace Sphere {
	GLuint sphereVao;
	GLuint sphereVbo;
	GLuint sphereShaders[3];
	GLuint sphereProgram;
	float radius;
	glm::mat4 objMat = glm::mat4(1.f);

	const char* sphere_vertShader =
		"#version 330\n\
		in vec3 in_Position;\n\
		uniform mat4 mv_Mat;\n\
		void main() {\n\
		gl_Position = mv_Mat * vec4(in_Position, 1.0);\n\
		}";
	const char* sphere_geomShader =
		"#version 330\n\
		layout(points) in;\n\
		layout(triangle_strip, max_vertices = 4) out;\n\
		out vec4 eyePos;\n\
		out vec4 centerEyePos;\n\
		uniform mat4 projMat;\n\
		uniform float radius;\n\
		vec4 nu_verts[4];\n\
		void main() {\n\
		vec3 n = normalize(-gl_in[0].gl_Position.xyz);\n\
		vec3 up = vec3(0.0, 1.0, 0.0);\n\
		vec3 u = normalize(cross(up, n));\n\
		vec3 v = normalize(cross(n, u));\n\
		nu_verts[0] = vec4(-radius*u - radius*v, 0.0); \n\
		nu_verts[1] = vec4( radius*u - radius*v, 0.0); \n\
		nu_verts[2] = vec4(-radius*u + radius*v, 0.0); \n\
		nu_verts[3] = vec4( radius*u + radius*v, 0.0); \n\
		centerEyePos = gl_in[0].gl_Position;\n\
		for (int i = 0; i < 4; ++i) {\n\
		eyePos = (gl_in[0].gl_Position + nu_verts[i]);\n\
		gl_Position = projMat * eyePos;\n\
		EmitVertex();\n\
		}\n\
		EndPrimitive();\n\
		}";
	const char* sphere_fragShader_flatColor =
		"#version 330\n\
		in vec4 eyePos;\n\
		in vec4 centerEyePos;\n\
		out vec4 out_Color;\n\
		uniform mat4 projMat;\n\
		uniform mat4 mv_Mat;\n\
		uniform vec4 color;\n\
		uniform float radius;\n\
		void main() {\n\
		vec4 diff = eyePos - centerEyePos;\n\
		float distSq2C = dot(diff, diff);\n\
		if (distSq2C > (radius*radius)) discard;\n\
		float h = sqrt(radius*radius - distSq2C);\n\
		vec4 nuEyePos = vec4(eyePos.xy, eyePos.z + h, 1.0);\n\
		vec4 nuPos = projMat * nuEyePos;\n\
		gl_FragDepth = ((nuPos.z / nuPos.w) + 1) * 0.5;\n\
		vec3 normal = normalize(nuEyePos - centerEyePos).xyz;\n\
		out_Color = vec4(color.xyz * dot(normal, (mv_Mat*vec4(0.0, 1.0, 0.0, 0.0)).xyz) + color.xyz * 0.3, 1.0 );\n\
		}";

	bool shadersCreated = false;
	void createSphereShaderAndProgram() {
		if (shadersCreated) return;

		sphereShaders[0] = compileShader(sphere_vertShader, GL_VERTEX_SHADER, "sphereVert");
		sphereShaders[1] = compileShader(sphere_geomShader, GL_GEOMETRY_SHADER, "sphereGeom");
		sphereShaders[2] = compileShader(sphere_fragShader_flatColor, GL_FRAGMENT_SHADER, "sphereFrag");

		sphereProgram = glCreateProgram();
		glAttachShader(sphereProgram, sphereShaders[0]);
		glAttachShader(sphereProgram, sphereShaders[1]);
		glAttachShader(sphereProgram, sphereShaders[2]);
		glBindAttribLocation(sphereProgram, 0, "in_Position");
		linkProgram(sphereProgram);

		shadersCreated = true;
	}
	void cleanupSphereShaderAndProgram() {
		if (!shadersCreated) return;
		glDeleteProgram(sphereProgram);
		glDeleteShader(sphereShaders[0]);
		glDeleteShader(sphereShaders[1]);
		glDeleteShader(sphereShaders[2]);
		shadersCreated = false;
	}

	void setupSphere(glm::vec3 pos, float radius) {
		Sphere::radius = radius;
		glGenVertexArrays(1, &sphereVao);
		glBindVertexArray(sphereVao);
		glGenBuffers(1, &sphereVbo);

		glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3, &pos, GL_DYNAMIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		createSphereShaderAndProgram();
	}
	void cleanupSphere() {
		glDeleteBuffers(1, &sphereVbo);
		glDeleteVertexArrays(1, &sphereVao);

		cleanupSphereShaderAndProgram();
	}
	void updateSphere(glm::vec3 pos, float radius) {
		glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
		float* buff = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
		buff[0] = pos.x;
		buff[1] = pos.y;
		buff[2] = pos.z;
		glUnmapBuffer(GL_ARRAY_BUFFER);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		Sphere::radius = radius;
	}
	void drawSphere() {
		glBindVertexArray(sphereVao);
		glUseProgram(sphereProgram);
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RV::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(RV::_projection));
		glUniform4f(glGetUniformLocation(sphereProgram, "color"), 0.6f, 0.1f, 0.1f, 1.f);
		glUniform1f(glGetUniformLocation(sphereProgram, "radius"), Sphere::radius);
		glDrawArrays(GL_POINTS, 0, 1);

		glUseProgram(0);
		glBindVertexArray(0);
	}

	void drawTrillionSpheres(glm::vec3 newPosition)
	{
		// Translate
		glm::mat4 translationMatrix = glm::translate(glm::mat4(), newPosition);		// Rampa movimiento
		RV::_MVP = translationMatrix;

		glBindVertexArray(sphereVao);
		glUseProgram(sphereProgram);
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RV::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(RV::_projection));
		glUniform4f(glGetUniformLocation(sphereProgram, "color"), 0.6f, 0.1f, 0.1f, 1.f);
		glUniform1f(glGetUniformLocation(sphereProgram, "radius"), Sphere::radius);
		glDrawArrays(GL_POINTS, 0, 1);

		glUseProgram(0);
		glBindVertexArray(0);
	}
}

////////////////////////////////////////////////// CAPSULE
namespace Capsule {
	GLuint capsuleVao;
	GLuint capsuleVbo[2];
	GLuint capsuleShader[3];
	GLuint capsuleProgram;
	float radius;

	const char* capsule_vertShader =
		"#version 330\n\
in vec3 in_Position;\n\
uniform mat4 mv_Mat;\n\
void main() {\n\
	gl_Position = mv_Mat * vec4(in_Position, 1.0);\n\
}";
	const char* capsule_geomShader =
		"#version 330\n\
layout(lines) in; \n\
layout(triangle_strip, max_vertices = 14) out;\n\
out vec3 eyePos;\n\
out vec3 capPoints[2];\n\
uniform mat4 projMat;\n\
uniform float radius;\n\
vec3 boxVerts[8];\n\
int boxIdx[14];\n\
void main(){\n\
	vec3 A = gl_in[0].gl_Position.xyz;\n\
	vec3 B = gl_in[1].gl_Position.xyz;\n\
	if(gl_in[1].gl_Position.x < gl_in[0].gl_Position.x) {\n\
		A = gl_in[1].gl_Position.xyz;\n\
		B = gl_in[0].gl_Position.xyz;\n\
	}\n\
	vec3 u = vec3(0.0, 1.0, 0.0);\n\
	if (abs(dot(u, normalize(gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz))) - 1.0 < 1e-6) {\n\
		if(gl_in[1].gl_Position.y > gl_in[0].gl_Position.y) {\n\
			A = gl_in[1].gl_Position.xyz;\n\
			B = gl_in[0].gl_Position.xyz;\n\
		}\n\
		u = vec3(1.0, 0.0, 0.0);\n\
	}\n\
	vec3 Am = normalize(A - B); \n\
	vec3 Bp = -Am;\n\
	vec3 v = normalize(cross(Am, u)) * radius;\n\
	u = normalize(cross(v, Am)) * radius;\n\
	Am *= radius;\n\
	Bp *= radius;\n\
	boxVerts[0] = A + Am - u - v;\n\
	boxVerts[1] = A + Am + u - v;\n\
	boxVerts[2] = A + Am + u + v;\n\
	boxVerts[3] = A + Am - u + v;\n\
	boxVerts[4] = B + Bp - u - v;\n\
	boxVerts[5] = B + Bp + u - v;\n\
	boxVerts[6] = B + Bp + u + v;\n\
	boxVerts[7] = B + Bp - u + v;\n\
	boxIdx = int[](0, 3, 4, 7, 6, 3, 2, 1, 6, 5, 4, 1, 0, 3);\n\
	capPoints[0] = A;\n\
	capPoints[1] = B;\n\
	for (int i = 0; i<14; ++i) {\n\
		eyePos = boxVerts[boxIdx[i]];\n\
		gl_Position = projMat * vec4(boxVerts[boxIdx[i]], 1.0);\n\
		EmitVertex();\n\
	}\n\
	EndPrimitive();\n\
}";
	const char* capsule_fragShader_flatColor =
		"#version 330\n\
in vec3 eyePos;\n\
in vec3 capPoints[2];\n\
out vec4 out_Color;\n\
uniform mat4 projMat;\n\
uniform mat4 mv_Mat;\n\
uniform vec4 color;\n\
uniform float radius;\n\
const int lin_steps = 30;\n\
const int bin_steps = 5;\n\
vec3 closestPointInSegment(vec3 p, vec3 a, vec3 b) {\n\
	vec3 pa = p - a, ba = b - a;\n\
	float h = clamp(dot(pa, ba) / dot(ba, ba), 0.0, 1.0);\n\
	return a + ba*h;\n\
}\n\
void main() {\n\
	vec3 viewDir = normalize(eyePos);\n\
	float step = radius / 5.0;\n\
	vec3 nuPB = eyePos;\n\
	int i = 0;\n\
	for(i = 0; i < lin_steps; ++i) {\n\
		nuPB = eyePos + viewDir*step*i;\n\
		vec3 C = closestPointInSegment(nuPB, capPoints[0], capPoints[1]);\n\
		float dist = length(C - nuPB) - radius;\n\
		if(dist < 0.0) break;\n\
	}\n\
	if(i==lin_steps) discard;\n\
	vec3 nuPA = nuPB - viewDir*step;\n\
	vec3 C;\n\
	for(i = 0; i < bin_steps; ++i) {\n\
		vec3 nuPC = nuPA + (nuPB - nuPA)*0.5; \n\
		C = closestPointInSegment(nuPC, capPoints[0], capPoints[1]); \n\
		float dist = length(C - nuPC) - radius; \n\
		if(dist > 0.0) nuPA = nuPC; \n\
		else nuPB = nuPC; \n\
	}\n\
	vec4 nuPos = projMat * vec4(nuPA, 1.0);\n\
	gl_FragDepth = ((nuPos.z / nuPos.w) + 1) * 0.5;\n\
	vec3 normal = normalize(nuPA - C);\n\
	out_Color = vec4(color.xyz * dot(normal, (mv_Mat*vec4(0.0, 1.0, 0.0, 0.0)).xyz) + color.xyz * 0.3, 1.0 );\n\
}";

	void setupCapsule(glm::vec3 posA, glm::vec3 posB, float radius) {
		Capsule::radius = radius;
		glGenVertexArrays(1, &capsuleVao);
		glBindVertexArray(capsuleVao);
		glGenBuffers(2, capsuleVbo);

		float capsuleVerts[] = {
			posA.x, posA.y, posA.z,
			posB.x, posB.y, posB.z
		};
		GLubyte capsuleIdx[] = {
			0, 1
		};

		glBindBuffer(GL_ARRAY_BUFFER, capsuleVbo[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6, capsuleVerts, GL_DYNAMIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, capsuleVbo[1]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte) * 2, capsuleIdx, GL_STATIC_DRAW);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		capsuleShader[0] = compileShader(capsule_vertShader, GL_VERTEX_SHADER, "capsuleVert");
		capsuleShader[1] = compileShader(capsule_geomShader, GL_GEOMETRY_SHADER, "capsuleGeom");
		capsuleShader[2] = compileShader(capsule_fragShader_flatColor, GL_FRAGMENT_SHADER, "capsuleFrag");

		capsuleProgram = glCreateProgram();
		glAttachShader(capsuleProgram, capsuleShader[0]);
		glAttachShader(capsuleProgram, capsuleShader[1]);
		glAttachShader(capsuleProgram, capsuleShader[2]);
		glBindAttribLocation(capsuleProgram, 0, "in_Position");
		linkProgram(capsuleProgram);
	}
	void cleanupCapsule() {
		glDeleteBuffers(2, capsuleVbo);
		glDeleteVertexArrays(1, &capsuleVao);

		glDeleteProgram(capsuleProgram);
		glDeleteShader(capsuleShader[0]);
		glDeleteShader(capsuleShader[1]);
		glDeleteShader(capsuleShader[2]);
	}
	void updateCapsule(glm::vec3 posA, glm::vec3 posB, float radius) {
		float vertPos[] = { posA.x, posA.y, posA.z, posB.z, posB.y, posB.z };
		glBindBuffer(GL_ARRAY_BUFFER, capsuleVbo[0]);
		float* buff = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
		buff[0] = posA.x; buff[1] = posA.y; buff[2] = posA.z;
		buff[3] = posB.x; buff[4] = posB.y; buff[5] = posB.z;
		glUnmapBuffer(GL_ARRAY_BUFFER);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		Capsule::radius = radius;
	}
	void drawCapsule() {
		glBindVertexArray(capsuleVao);
		glUseProgram(capsuleProgram);
		glUniformMatrix4fv(glGetUniformLocation(capsuleProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
		glUniformMatrix4fv(glGetUniformLocation(capsuleProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RV::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(capsuleProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(RV::_projection));
		glUniform4fv(glGetUniformLocation(capsuleProgram, "camPoint"), 1, &RV::_cameraPoint[0]);
		glUniform4f(glGetUniformLocation(capsuleProgram, "color"), 0.1f, 0.6f, 0.1f, 1.f);
		glUniform1f(glGetUniformLocation(capsuleProgram, "radius"), Capsule::radius);
		glDrawElements(GL_LINES, 2, GL_UNSIGNED_BYTE, 0);

		glUseProgram(0);
		glBindVertexArray(0);
	}
}

////////////////////////////////////////////////// CUBE
namespace Cube {
	GLuint cubeVao;
	GLuint cubeVbo[3];
	GLuint cubeShaders[2];
	GLuint cubeProgram;
	glm::mat4 objMat = glm::mat4(1.f);

	float extraHeight = 0;
	float superiorHeight = 1.1f;
	float intermediateHeight = 0.75;
	float inferiorHeight = 0.45f;
	float waterLevel = 0.2f;
	float waterDifference = 0;

	extern const float halfW = 0.5f;
	int numVerts = 24 + 6; // 4 vertex/face * 6 faces + 6 PRIMITIVE RESTART

						   //   4---------7
						   //  /|        /|
						   // / |       / |
						   //5---------6  |
						   //|  0------|--3
						   //| /       | /
						   //|/        |/
						   //1---------2
	glm::vec3 verts[] = {
		glm::vec3(-halfW, -halfW, -halfW),
		glm::vec3(-halfW, -halfW,  halfW),
		glm::vec3(halfW, -halfW,  halfW),
		glm::vec3(halfW, -halfW, -halfW),
		glm::vec3(-halfW,  halfW, -halfW),
		glm::vec3(-halfW,  halfW,  halfW),
		glm::vec3(halfW,  halfW,  halfW),
		glm::vec3(halfW,  halfW, -halfW)
	};
	glm::vec3 norms[] = {
		glm::vec3(0.f, -1.f,  0.f),
		glm::vec3(0.f,  1.f,  0.f),
		glm::vec3(-1.f,  0.f,  0.f),
		glm::vec3(1.f,  0.f,  0.f),
		glm::vec3(0.f,  0.f, -1.f),
		glm::vec3(0.f,  0.f,  1.f)
	};

	glm::vec3 cubeVerts[] = {
		verts[1], verts[0], verts[2], verts[3],
		verts[5], verts[6], verts[4], verts[7],
		verts[1], verts[5], verts[0], verts[4],
		verts[2], verts[3], verts[6], verts[7],
		verts[0], verts[4], verts[3], verts[7],
		verts[1], verts[2], verts[5], verts[6]
	};
	glm::vec3 cubeNorms[] = {
		norms[0], norms[0], norms[0], norms[0],
		norms[1], norms[1], norms[1], norms[1],
		norms[2], norms[2], norms[2], norms[2],
		norms[3], norms[3], norms[3], norms[3],
		norms[4], norms[4], norms[4], norms[4],
		norms[5], norms[5], norms[5], norms[5]
	};
	GLubyte cubeIdx[] = {
		0, 1, 2, 3, UCHAR_MAX,
		4, 5, 6, 7, UCHAR_MAX,
		8, 9, 10, 11, UCHAR_MAX,
		12, 13, 14, 15, UCHAR_MAX,
		16, 17, 18, 19, UCHAR_MAX,
		20, 21, 22, 23, UCHAR_MAX
	};




	const char* cube_vertShader =
		"#version 330\n\
	in vec3 in_Position;\n\
	in vec3 in_Normal;\n\
	out vec4 vert_Normal;\n\
	uniform mat4 objMat;\n\
	uniform mat4 mv_Mat;\n\
	uniform mat4 mvpMat;\n\
	void main() {\n\
		gl_Position = mvpMat * objMat * vec4(in_Position, 1.0);\n\
		vert_Normal = mv_Mat * objMat * vec4(in_Normal, 0.0);\n\
	}";


	const char* cube_fragShader =
		"#version 330\n\
		in vec4 vert_Normal;\n\
		out vec4 out_Color;\n\
		uniform mat4 mv_Mat;\n\
		uniform vec4 color;\n\
		void main() {\n\
			out_Color = vec4(color.xyz * dot(vert_Normal, mv_Mat*vec4(0.0, 1.0, 0.0, 0.0)) + color.xyz * 0.3, 1.0 );\n\
		}";
	void setupCube() {
		glGenVertexArrays(1, &cubeVao);
		glBindVertexArray(cubeVao);
		glGenBuffers(3, cubeVbo);

		glBindBuffer(GL_ARRAY_BUFFER, cubeVbo[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVerts), cubeVerts, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, cubeVbo[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNorms), cubeNorms, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);

		glPrimitiveRestartIndex(UCHAR_MAX);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeVbo[2]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeIdx), cubeIdx, GL_STATIC_DRAW);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		cubeShaders[0] = compileShader(cube_vertShader, GL_VERTEX_SHADER, "cubeVert");
		cubeShaders[1] = compileShader(cube_fragShader, GL_FRAGMENT_SHADER, "cubeFrag");

		cubeProgram = glCreateProgram();
		glAttachShader(cubeProgram, cubeShaders[0]);
		glAttachShader(cubeProgram, cubeShaders[1]);
		glBindAttribLocation(cubeProgram, 0, "in_Position");
		glBindAttribLocation(cubeProgram, 1, "in_Normal");
		linkProgram(cubeProgram);
	}
	void cleanupCube() {
		glDeleteBuffers(3, cubeVbo);
		glDeleteVertexArrays(1, &cubeVao);

		glDeleteProgram(cubeProgram);
		glDeleteShader(cubeShaders[0]);
		glDeleteShader(cubeShaders[1]);
	}
	void updateCube(const glm::mat4& transform) {
		objMat = transform;
	}
	void drawCube() {
		glEnable(GL_PRIMITIVE_RESTART);
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.1f, 1.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		glUseProgram(0);
		glBindVertexArray(0);
		glDisable(GL_PRIMITIVE_RESTART);
	}

	void drawCube(glm::vec3 newPosition)
	{
		// Translate
		glm::mat4 translationMatrix = glm::translate(glm::mat4(), newPosition);
		objMat = translationMatrix;

		glEnable(GL_PRIMITIVE_RESTART);
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.1f, 1.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		glUseProgram(0);
		glBindVertexArray(0);
		glDisable(GL_PRIMITIVE_RESTART);
	}

	void draw2Cubes(double currentTime)
	{
		// Cube - 1 

		glEnable(GL_PRIMITIVE_RESTART);
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);

		// Red to black
		float red = 10 * (float)sin(currentTime * 2) * 0.5f + 0.5f;


		// Translate
		//glm::mat4 translationMatrix = glm::translate(glm::mat4(), glm::vec3(-1, red + 5, 0.0f));		// Sin movement
		glm::mat4 translationMatrix = glm::translate(glm::mat4(), glm::vec3(fmod(currentTime, 5), 2, 0.0f));		// Rampa movimiento

		objMat = translationMatrix;

		// Rotate
		//translationMatrix = glm::rotate(translationMatrix, red, glm::vec3(0, 1, 0));
		//objMat = translationMatrix;

		// Scale
		float scaleFactor = (float)sin(currentTime * 2) * 0.75f + 1.5f;

		glm::mat4 scaleMatrix = glm::scale(glm::mat4(), glm::vec3(scaleFactor, scaleFactor, scaleFactor));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), red, 0.f, 0.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Cube - 2

		// Translate
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(1, 2.0, 0.0f));
		objMat = translationMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));

		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, 1.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		glUseProgram(0);
		glBindVertexArray(0);
		glDisable(GL_PRIMITIVE_RESTART);
	}

	void drawScene(double currentTime)
	{

		// *******
		// PILAR *
		// *******

		// Body
		glm::mat4 translationMatrix = glm::translate(glm::mat4(), glm::vec3(-0.65, 2.5 + extraHeight, 2.3));
		objMat = translationMatrix;

		glm::mat4 scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.25, 2, 0.25));
		objMat = translationMatrix * scaleMatrix;

		glEnable(GL_PRIMITIVE_RESTART);
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 1.f, 1.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		// Base
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-0.65, 1.7 + extraHeight, 2.3));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.4, 0.3, 0.4));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 1.f, 1.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Floating sun
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-0.65, 3.9 + extraHeight, 2.3));
		objMat = translationMatrix;

		translationMatrix = glm::rotate(translationMatrix, (float)currentTime, glm::vec3(1, 1, 1));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.3, 0.3, 0.3));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 1.f, 1.f, 0.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// ********
		// HOUSES *
		// ********

		// House - 1
		// Body

		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-1, 1.75 + extraHeight, -4));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1, 1, 1));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 1.f, 1.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Roof
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-1, 2.4 + extraHeight, -4));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1, 0.3, 1));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .2f, 0.2f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		// Left Window
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-1.3, 2.0 + extraHeight, -3.6));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.25, 0.25, 0.25));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.0f, .8f, 0.9f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		// Right Window
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-0.7, 2.0 + extraHeight, -3.6));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.25, 0.25, 0.25));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.0f, .8f, 0.9f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Door
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-1, 1.55 + extraHeight, -3.6));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.25, 0.55, 0.25));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.7f, .3f, 0.0f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		// Chimney
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-0.75, 2.6 + extraHeight, -4));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.3, 0.3, 0.3));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.8f, .8f, 0.8f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// House - 2
		// Body

		translationMatrix = glm::translate(glm::mat4(), glm::vec3(4, 1.75 + extraHeight, -2.8));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1, 1, 1));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 1.f, 1.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Roof
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(4, 2.4 + extraHeight, -2.8));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1, 0.3, 1));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .2f, 0.2f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		// Left Window
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(3.7, 2.0 + extraHeight, -2.4));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.25, 0.25, 0.25));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.0f, .8f, 0.9f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		// Right Window
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(4.3, 2.0 + extraHeight, -2.4));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.25, 0.25, 0.25));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.0f, .8f, 0.9f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Door
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(4, 1.55 + extraHeight, -2.4));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.25, 0.55, 0.25));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.7f, .3f, 0.0f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		// Chimney
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(3.75, 2.6 + extraHeight, -2.8));
		objMat = translationMatrix;

		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.3, 0.3, 0.3));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.8f, .8f, 0.8f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// ****************
		// INFERIOR LEVEL *
		// ****************

		// Cube - 1
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(0, inferiorHeight + extraHeight, -3.3));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(10, 1, 3.4));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Cube - 2
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(3.75, inferiorHeight + extraHeight, -0.5));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(2.5, 1, 2.4));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		// Cube - 3
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-3.0, inferiorHeight + extraHeight, -1.2));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(4, 1, 0.8));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Cube - 4
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-3.0, inferiorHeight + extraHeight, -0.5));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1.7, 1, 0.8));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Cube - 5
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(2.1, inferiorHeight + extraHeight, -1.0));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.8, 1, 1.3));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Cube - 6
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(4.6, inferiorHeight + extraHeight, 1.1));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.8, 1, 1.3));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Cube - 8
		// Translate
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(0.4f, inferiorHeight + extraHeight, 2.5f));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1.2, 1, 2.2));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		
		// Cube - 9
		// Translate
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-0.6f, inferiorHeight + extraHeight, 2.3f));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.85, 1, 1.8));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		
		// Cube - 10
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-1.45f, inferiorHeight + extraHeight, 2.6f));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.9, 1, 2.4));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Cube - 11
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-2.35f, inferiorHeight + extraHeight, 2.4f));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.9, 1, 1.5));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		// Cube - 12
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-3.15, inferiorHeight + extraHeight, 2.9));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.75, 1, 1.9));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		// Cube - 13
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-4.25, inferiorHeight + extraHeight, 2.9));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1.5, 1, 3.4));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.9f, .3f, 0.f, 0.5f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// *********************
		// IINTERMEDIATE LEVEL *
		// *********************

		{
			// Cube - 1
			translationMatrix = glm::translate(glm::mat4(), glm::vec3(-3, intermediateHeight + extraHeight, -3.75));
			objMat = translationMatrix;

			// Scale
			scaleMatrix = glm::scale(glm::mat4(), glm::vec3(4, 1, 2.5));
			objMat = translationMatrix * scaleMatrix;

			glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniform4f(glGetUniformLocation(cubeProgram, "color"), .9f, 0.5f, 0.f, 0.f);
			glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


			// Cube - 2
			translationMatrix = glm::translate(glm::mat4(), glm::vec3(1, intermediateHeight + extraHeight, -4.05));
			objMat = translationMatrix;

			// Scale
			scaleMatrix = glm::scale(glm::mat4(), glm::vec3(4, 1, 1.9));
			objMat = translationMatrix * scaleMatrix;

			glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniform4f(glGetUniformLocation(cubeProgram, "color"), .9f, 0.5f, 0.f, 0.f);
			glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


			// Cube - 3
			translationMatrix = glm::translate(glm::mat4(), glm::vec3(3.95, intermediateHeight + extraHeight, -3));
			objMat = translationMatrix;

			// Scale
			scaleMatrix = glm::scale(glm::mat4(), glm::vec3(2.1, 1, 4));
			objMat = translationMatrix * scaleMatrix;

			glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniform4f(glGetUniformLocation(cubeProgram, "color"), .9f, 0.5f, 0.f, 0.f);
			glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


			// Cube - 4
			translationMatrix = glm::translate(glm::mat4(), glm::vec3(4.3, intermediateHeight + extraHeight, -0.7));
			objMat = translationMatrix;

			// Scale
			scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1.4, 1, 1));
			objMat = translationMatrix * scaleMatrix;

			glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniform4f(glGetUniformLocation(cubeProgram, "color"), .9f, 0.5f, 0.f, 0.f);
			glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);



			// Cube - 5
			translationMatrix = glm::translate(glm::mat4(), glm::vec3(-3.65, intermediateHeight + extraHeight, -2));
			objMat = translationMatrix;

			// Scale
			scaleMatrix = glm::scale(glm::mat4(), glm::vec3(2.7, 1, 1));
			objMat = translationMatrix * scaleMatrix;

			glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniform4f(glGetUniformLocation(cubeProgram, "color"), .9f, 0.5f, 0.f, 0.f);
			glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


			// Cube - 6
			translationMatrix = glm::translate(glm::mat4(), glm::vec3(2.5, intermediateHeight + extraHeight, -2.7));
			objMat = translationMatrix;

			// Scale
			scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1, 1, 1));
			objMat = translationMatrix * scaleMatrix;

			glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniform4f(glGetUniformLocation(cubeProgram, "color"), .9f, 0.5f, 0.f, 0.f);
			glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

			// Cube - 7
			translationMatrix = glm::translate(glm::mat4(), glm::vec3(-0.65, intermediateHeight + extraHeight, 2.3));
			objMat = translationMatrix;

			// Scale
			scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1.8, 1, 1.2));
			objMat = translationMatrix * scaleMatrix;

			glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniform4f(glGetUniformLocation(cubeProgram, "color"), .9f, 0.5f, 0.f, 0.f);
			glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);



			// Cube - 8
			translationMatrix = glm::translate(glm::mat4(), glm::vec3(-4.1, intermediateHeight + extraHeight, 3));
			objMat = translationMatrix;

			// Scale
			scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1.8, 1, 1));
			objMat = translationMatrix * scaleMatrix;

			glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniform4f(glGetUniformLocation(cubeProgram, "color"), .9f, 0.5f, 0.f, 0.f);
			glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


			// Cube - 9
			translationMatrix = glm::translate(glm::mat4(), glm::vec3(-4.475, intermediateHeight + extraHeight, 2.4));
			objMat = translationMatrix;

			// Scale
			scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1.05, 1, 1));
			objMat = translationMatrix * scaleMatrix;

			glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniform4f(glGetUniformLocation(cubeProgram, "color"), .9f, 0.5f, 0.f, 0.f);
			glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


			// Cube - 10
			translationMatrix = glm::translate(glm::mat4(), glm::vec3(0.4, intermediateHeight + extraHeight, 2.85));
			objMat = translationMatrix;

			// Scale
			scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.7, 1, 0.7));
			objMat = translationMatrix * scaleMatrix;

			glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniform4f(glGetUniformLocation(cubeProgram, "color"), .9f, 0.5f, 0.f, 0.f);
			glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		}


		// ****************
		// SUPERIOR LEVEL *
		// ****************

		// Cube - 1
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-3.5, superiorHeight + extraHeight, -4));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(2, 1, 1));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), .6f, 0.7f, .2f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Cube - 3
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-2.65, superiorHeight + extraHeight, -3.4));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1, 1, 1));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), .6f, 0.7f, .2f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Cube - 2
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(1.1, superiorHeight + extraHeight, -4.2));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1.3, 1, 0.8));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), .6f, 0.7f, .2f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Cube - 4
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-0.65, superiorHeight + extraHeight, 2.3));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(1.1, 1, 0.6));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), .6f, 0.7f, .2f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// Cube - 5
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(-4.45, superiorHeight + extraHeight, 2.7));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.6, 0.9, 1.1));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), .6f, 0.7f, .2f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		// *******
		// WATER *
		// *******

		// Complex

		for (int j = 0; j < 20; j++)
		{
			waterDifference = 0;

			for (int i = 0; i < 20; i++)
			{
				translationMatrix = glm::translate(glm::mat4(), glm::vec3((-5 + 0.25) + (0.5 * i), (waterLevel + extraHeight) + (float)sin((currentTime + waterDifference) * 1.5) * 0.1f, (-5 + 0.25) + (0.5 * j)));				objMat = translationMatrix;

				scaleMatrix = glm::scale(glm::mat4(), glm::vec3(0.5, 1, 0.5));
				objMat = translationMatrix * scaleMatrix;

				glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
				glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, 1.f, 1.f, 0.f);
				glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

				waterDifference += 0.2f;
			}
		}


		// Simple water
		/*
		translationMatrix = glm::translate(glm::mat4(), glm::vec3(0, 0.32, 0));
		objMat = translationMatrix;

		// Scale
		scaleMatrix = glm::scale(glm::mat4(), glm::vec3(10, 0.7, 10));
		objMat = translationMatrix * scaleMatrix;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, 1.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);
		*/
		




		glUseProgram(0);
		glBindVertexArray(0);
		glDisable(GL_PRIMITIVE_RESTART);
	}
}